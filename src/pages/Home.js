import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

	const data = {
		title: "Zurich Horology",
		content: "Timeless elegance on your wrist: Discover our exquisite collection of luxury watches",
		destination: "/watches",
		label: "shop now"
	}

	return (
		<>
			<Banner data={data} />
		
			<Highlights />
		</>
	)
}
