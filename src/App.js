import {useState, useEffect} from 'react';

import ProductView from './components/ProductView';
import UpdateView from './components/UpdateView';
import AppNavbar from './components/AppNavbar';




import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Home from './pages/Home';
import Dashboard from './pages/Dashboard';
import Error from './pages/Error';

import {Container} from 'react-bootstrap';

import {UserProvider} from './UserContext';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import './App.css'; 

function App() {

const [user, setUser] = useState({
    id: null,
    isAdmin: null
});


  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem(`token`)}`
      }
    })
    .then(res => res.json())
    .then(data => {

      // user is logged in
      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
        //user is logged out
      else{
        setUser({
          id: null,
          isAdmin: null
        })
      }

    })
  }, []);

  return (
    // <></> fragments - common pattern in React for component to return multiple elements
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          < AppNavbar/>
          <Container>
          <Routes>

            < Route path="/register" element={<Register/>}/>
            < Route path="/login" element={<Login />}/>
            < Route path="/logout" element={<Logout />}/>
            < Route path="/" element={<Home />}/>
            < Route path="/dashboard" element={<Dashboard />}/>
            < Route path="/products" element={<Products/>}/>
            < Route path="/products/:productId" element={<ProductView/>}/>
            < Route path="/products/:productId/update" element={<UpdateView/>}/>
            < Route path="/*" element={<Error />} /> 
          </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
