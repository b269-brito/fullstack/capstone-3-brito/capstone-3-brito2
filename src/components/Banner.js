import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {
    const {title, content, destination, label} = data;

    return (
        <div className="home">
        <Row>
            <Col className="p-5 d-flex flex-column justify-content-center align-items-center">
                <h1 className= "text-center">{title}</h1>
                <p className="hometext text-center">{content}</p>
                <Button style={{backgroundColor: "black"}} variant="primary" as={Link} to={`/products`} >{label}</Button>
            </Col>
        </Row>
        </div>
    )
}
