import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function ProductView() {
	
	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const {productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);


	const addcart = () => {
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				
				products: [productId]
			})
		})
		.then(data => {
			console.log(data)

			if(data.status === 200) {
				Swal.fire({
					title: "Thank you for purchasing!",
					icon: "success",
					text: "Kindly expect an email for shipment details"
				})

				navigate("/products")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}

		})
	};


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
			})
	}, [productId])

	return (

		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        <Card.Subtitle>Payment Method:</Card.Subtitle>
					        <Card.Text>Cash Only</Card.Text>
					        
					        {
					        	(user.id !== null) ?
					        		<Button variant="primary" onClick={() => addcart()} >Buy now!</Button>
					        		:
					        		<Button className="btn btn-danger" as={Link} to="/login"  >Login to purchase</Button>
					        }

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}
