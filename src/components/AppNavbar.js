import React, { useState, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';


export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    
    <Navbar class="navbar" bg="light" expand="lg">
      <Container>
      <div className="nav">
        <Navbar.Brand id="navbarLogo"></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
          {user.isAdmin === true ? (
            <Nav.Link as={NavLink} to="/dashboard">Dashboard</Nav.Link>
            ): null}

            {user && user.isAdmin === true ? (
              <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
              
            ) : (
              <>
                <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
                {user && user.id !== null ? (
                  <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                ) : (
                  <>
                    <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                    <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                  </>
                )}
              </>
            )}
          </Nav>
        </Navbar.Collapse>
        </div>
      </Container>
    </Navbar>

  );
}
